package pl.edu.pwsztar;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddProduct {

    @DisplayName("Should add Products to Cart")
    @ParameterizedTest
    @CsvSource({
            "Apple, 14, 1",
            "Orange, 7, 3",
            "Milk, 1, 2"
    })
    void shouldAddProducts(String productName, int price, int quantity) {
        //given
        final ShoppingCart shoppingCart = new ShoppingCart();
        //when
        final boolean result = shoppingCart.addProducts(productName, price, quantity);
        //then
        assertTrue(result);
    }

    @DisplayName("Should not add Products to Cart")
    @ParameterizedTest
    @CsvSource({
            "Apple, 14, -2",
            "Orange, 0, 3",
            "Milk, 1, -2"
    })
    void shouldNotAddProducts(String productName, int price, int quantity) {
        //given
        final ShoppingCart shoppingCart = new ShoppingCart();
        //when
        final boolean result = shoppingCart.addProducts(productName, price, quantity);
        //then
        assertFalse(result);
    }
}
