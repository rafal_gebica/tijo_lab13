package pl.edu.pwsztar;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetProductsNames {
    @DisplayName("Should return names of products from Cart")
    @Test
    void shouldReturnProductsList() {
        //given
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple", 12, 2);
        shoppingCart.addProducts("Orange", 12, 4);
        shoppingCart.addProducts("Milk", 12, 5);

        List<String> products = new ArrayList<>();
        products.add("Apple");
        products.add("Orange");
        products.add("Milk");
        //when
        final List<String> result = shoppingCart.getProductsNames();
        //then
        assertEquals(products, result);
    }
}
