package pl.edu.pwsztar;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetQuantityOfProduct {
    @DisplayName("Should return quantity of Orange from Shopping Cart")
    @ParameterizedTest
    @CsvSource({
            "Orange",
    })
    void shouldReturnProductQuantity(String productName) {
        //given
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Orange", 12, 5);
        //when
        final int result = shoppingCart.getQuantityOfProduct(productName);
        //then
        assertEquals(5, result);
    }
}