package pl.edu.pwsztar;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetSumProductPrices {
    @DisplayName("Should return sum price of products from Cart")
    @Test
    void shouldReturnProductsSumPrice() {
        //given
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Orange", 12, 5);
        shoppingCart.addProducts("Apple", 12, 3);
        shoppingCart.addProducts("Milk", 12, 2);
        //when
        final int result = shoppingCart.getSumProductsPrices();
        //then
        assertEquals(120, result);
    }
}
