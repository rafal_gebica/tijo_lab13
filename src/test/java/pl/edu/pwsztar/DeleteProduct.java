package pl.edu.pwsztar;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DeleteProduct {
    @DisplayName("Should delete product from Cart")
    @ParameterizedTest
    @CsvSource({
            "Apple, 1",
            "Orange,  2",
            "Milk, 1"
    })
    void shouldDeleteProducts(String productName, int quantity) {
        //given
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple", 14, 1);
        shoppingCart.addProducts("Orange", 7, 3);
        shoppingCart.addProducts("Milk", 1, 2);
        //when
        final boolean result = shoppingCart.deleteProducts(productName, quantity);
        //then
        assertTrue(result);
    }

    @DisplayName("Should not delete product from Shopping Cart")
    @ParameterizedTest
    @CsvSource({
            "Apple, 2",
            "Orange,  4",
            "Milk, 5"
    })
    void shouldNotDelete(String productName, int quantity) {
        //given
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple", 14, 1);
        shoppingCart.addProducts("Orange", 7, 3);
        shoppingCart.addProducts("Milk", 1, 2);
        //when
        final boolean result = shoppingCart.deleteProducts(productName, quantity);
        //then
        assertFalse(result);
    }

    @DisplayName("Should not delete product from Cart")
    @ParameterizedTest
    @CsvSource({
            "    ,   2",
            "Orange,  -4",
            "jeden ,   5"
    })
    void shouldNotDeleteWithWrongData(String productName, int quantity) {
        //given
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple", 14, 1);
        shoppingCart.addProducts("Orange", 7, 3);
        shoppingCart.addProducts("Milk", 1, 2);
        //when
        final boolean result = shoppingCart.deleteProducts(productName, quantity);
        //then
        assertFalse(result);
    }
}
