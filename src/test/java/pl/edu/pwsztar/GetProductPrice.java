package pl.edu.pwsztar;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetProductPrice {
    @DisplayName("Should return price of Orange from Shopping Cart")
    @ParameterizedTest
    @CsvSource({
            "Orange",
    })
    void shouldReturnPrice(String productName) {
        //given
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Orange", 12, 5);
        //when
        final int result = shoppingCart.getProductPrice(productName);
        //then
        assertEquals(12, result);
    }
}
