package pl.edu.pwsztar;

public class Main {

    public static void main( String[] argv ) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Orange", 12, 5);
        shoppingCart.addProducts("Apple", 12, 3);
        shoppingCart.addProducts("Milk", 12, 2);

        System.out.println("Products quantity " + shoppingCart.getProductsQuantity()); //10
        System.out.println("Products sum price " + shoppingCart.getSumProductsPrices()); //120
        System.out.println("Orange quantity " + shoppingCart.getQuantityOfProduct("Orange")); //5
        System.out.println("Orange price " + shoppingCart.getProductPrice("Orange")); //12
        System.out.println(shoppingCart.getProductPrice(""));
        System.out.println("Products list " + shoppingCart.getProductsNames());
    }
}
