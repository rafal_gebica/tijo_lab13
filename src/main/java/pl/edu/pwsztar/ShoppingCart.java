package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    private final List<Product> productsList = new ArrayList<>();

    public boolean addProducts(String productName, int price, int quantity) {
        if (getProductsQuantity() + quantity > PRODUCTS_LIMIT || quantity <= 0 || price <= 0) {
            System.out.println("Unsupported value in" + productName);
            return false;
        }
        if (isAlreadyInCart(productName)) {
            for (Product product : productsList) {
                if (product.getProductName().equals(productName)) {
                    product.setQuantity(product.getQuantity() + quantity);
                    return true;
                }
            }
        } else {
            productsList.add(new Product(productName, price, quantity));
            return true;
        }
        return false;
    }

    public boolean deleteProducts(String productName, int quantity) {
        if(quantity <=0 || productName == null){
            return false;
        }
        for(Product product: productsList){
            if(product.getProductName().equals(productName) && product.getQuantity() >= quantity) {
                product.setQuantity(product.getQuantity() - quantity);
                return true;
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        if (productName.equals("")) {
            return 0;
        }
        return productsList.stream()
                .filter(product -> product.getProductName().equals(productName))
                .mapToInt(Product::getQuantity)
                .sum();
    }

    public int getSumProductsPrices() {
        return productsList.stream().mapToInt(product -> product.getPrice() * product.getQuantity())
                .sum();
    }

    public int getProductPrice(String productName) {
        if (productName.equals("")) {
            return 0;
        }
        return productsList.stream()
                .filter(product -> product.getProductName().equals(productName))
                .mapToInt(Product::getPrice)
                .sum();
    }

    public List<String> getProductsNames() {
        return productsList.stream()
                .map(Product::getProductName)
                .collect(Collectors.toList());
    }

    private boolean isAlreadyInCart(String productName) {
        return productsList.stream()
                .anyMatch(product -> product.getProductName()
                        .equals(productName));
    }

    public int getProductsQuantity(){
        return productsList.stream()
                .mapToInt(Product::getQuantity)
                .sum();
    }
}
